# Mainzelliste.Client

Mainzelliste (see <http://www.mainzelliste.de>) is a web-based patient list and pseudonymization service. It provides a RESTful interface for client applications like electronic data capture (EDC) systems.

Mainzelliste-Client handles the HTTP calls necessary for using Mainzelliste from a Java application. It has been released as part of the Open Source Registry System for Rare Diseases in the EU (see <http://osse-register.de>).

As of November 2nd, 2016, the Mainzelliste repository has moved to https://bitbucket.org/medicalinformatics/mainzelliste.client. Please see the following help article on how to change the repository location in your working copies:

* https://help.github.com/articles/changing-a-remote-s-url/

If you have forked Mainzelliste.Client in Bitbucket, the fork is now linked to the new location automatically. Still, you should change the location in your local copies (usually, the origin of the fork is configured as a remote with name "upstream" when cloning from Bitbucket).
  
